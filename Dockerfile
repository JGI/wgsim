FROM debian:jessie

Maintainer Kecia Duffy, kmduffy@lbl.gov

# Set the DEBIAN_FRONTEND environment variable as shown to avoid problems
# being prompted for input
#
ENV DEBIAN_FRONTEND=noninteractive

RUN apt-get update -y  && \
    apt-get install -y wget ca-certificates --no-install-recommends && \
	apt-get install make  && \
	apt-get install gcc  -y --no-install-recommends && \
    apt-get install libc6-dev -y --no-install-recommends && \
	apt-get install git -y && \
	apt-get install zlib1g-dev   && \
	apt-get purge -y --auto-remove 

RUN git clone https://github.com/lh3/wgsim 
    WORKDIR wgsim 
RUN gcc -g -O2 -Wall -o wgsim wgsim.c -lz -lm && \
    chmod 755 wgsim && \
    mv /wgsim/wgsim /usr/bin/wgsim
